package aula20190430.introducao_a_sockets;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
	
	public static void main(String[] args) {
		Client c = new Client();
		c.start();
	}

	private void start() {
		
		try {
			Scanner input = new Scanner(System.in);
			String comando = "";
			
			do {
				System.out.println("===> Digite 1 para conectar ");
				comando = input.nextLine();
				
				if (comando.equals("1")) {
					try {
						Socket server = new Socket("localhost", 9092);
						Scanner reader =  new Scanner(server.getInputStream());
						PrintWriter toServer = new PrintWriter(server.getOutputStream());
						String resposta = reader.nextLine();
						System.out.println("Resposta do servidor: " + resposta);
						
						do {
							System.out.println("DIGITA AI");
							String comandoTeste = input.nextLine();
							toServer.println(comandoTeste);
							toServer.flush();
							String resposta2 = reader.nextLine();
							System.out.println("Respondeu" + resposta2);
							
						} while (true);
					} catch (Exception e) {
						comando = "0";
						e.printStackTrace();
						throw new Error("Houve um problema na conex�o com o socket");
					}
				}
				System.out.println(comando);
				
			} while (!comando.equals("0"));
			
			System.out.println("Saiu ashuduha");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
